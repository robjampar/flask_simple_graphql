from setuptools import setup, find_packages

required_packages = ["graphql-core>=2.1", "flask>=0.7.0", "graphql-server-core>=1.1"]

setup(
    name='flask_simple_graphql',
    version='1.0.1',
    packages=find_packages(),
    include_package_data=True,
    author='Robert Parker',
    long_description='GraphQL helpers for Flask',
    install_requires=required_packages
)
