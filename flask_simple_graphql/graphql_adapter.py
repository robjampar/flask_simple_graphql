import os
from functools import partial
from typing import Any, Type

from flask import Flask, Response, request as flask_request

from graphql.type.schema import GraphQLSchema
from graphql_server import (HttpQueryError, default_format_error,
                            encode_execution_results, json_encode,
                            load_json_body, run_http_query)


class GraphQLAdapter:

    @classmethod
    def from_root(cls, root: Type) -> 'GraphQLAdapter':
        try:
            from objectql import GraphQLSchemaBuilder
        except ImportError:
            raise ImportError('To create an GraphQLAdapter from a root type, ObjectQL must be installed')

        schema, _, root_value = GraphQLSchemaBuilder(root=root).schema()
        return GraphQLAdapter(schema=schema, root_value=root_value)

    def __init__(self, schema: GraphQLSchema, root_value: Any, graphiql: bool = True):
        self.schema = schema
        self.root_value = root_value
        self.graphiql = graphiql

    format_error = staticmethod(default_format_error)
    encode = staticmethod(json_encode)

    def dispatch(self, request):
        try:
            request_method = request.method.lower()
            data = self.parse_body(request=request)

            show_graphiql = request_method == 'get' and self.should_display_graphiql(request=request)

            if show_graphiql:
                graphiql_path = os.path.join(os.path.dirname(__file__), 'graphiql.html')
                return open(graphiql_path, 'r').read()

            execution_results, all_params = run_http_query(
                self.schema,
                request_method,
                data,
                query_data=request.args,
                root=self.root_value,
            )
            result, status_code = encode_execution_results(
                execution_results,
                is_batch=isinstance(data, list),
                format_error=self.format_error,
                encode=partial(self.encode)
            )

            if show_graphiql:
                return render_graphiql(
                    params=all_params[0],
                    result=result
                )

            return Response(
                result,
                status=status_code,
                content_type='application/json'
            )

        except HttpQueryError as e:
            return Response(
                self.encode({
                    'errors': [self.format_error(e)]
                }),
                status=e.status_code,
                headers=e.headers,
                content_type='application/json'
            )

    def parse_body(self, request):
        content_type = request.mimetype
        if content_type == 'application/graphql':
            return {'query': request.data.decode('utf8')}

        elif content_type == 'application/json':
            return load_json_body(request.data.decode('utf8'))

        elif content_type in ('application/x-www-form-urlencoded', 'multipart/form-data'):
            return request.form

        return {}

    def should_display_graphiql(self, request):
        if not self.graphiql or 'raw' in request.args:
            return False

        return self.request_wants_html(request=request)

    def request_wants_html(self, request):
        best = request.accept_mimetypes \
            .best_match(['application/json', 'text/html'])
        return best == 'text/html' and \
               request.accept_mimetypes[best] > \
               request.accept_mimetypes['application/json']

    def app(self):
        app = Flask(__name__)

        @app.route("/", methods=["GET", "POST"])
        def index():
            return self.dispatch(request=flask_request)

        return app